<?php

namespace Artemadr\VkAds;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\Plans\Plans;
use Artemadr\VkAds\Methods\Groups\Groups;
use Artemadr\VkAds\Methods\LeadAds\LeadAds;
use Artemadr\VkAds\Methods\Packages\Packages;
use Artemadr\VkAds\Methods\PostInterface;
use Artemadr\VkAds\Methods\Statistics\Statistics;
use Artemadr\VkAds\Methods\Banners\Banners;

class VkAdsApi
{
    private string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function leadAds(): LeadAds
    {
        return new LeadAds($this->token);
    }

    public function statistics(): Statistics
    {
        return new Statistics($this->token);
    }

    public function plans(string $version): GetInterface
    {
        return (new Plans($this->token))->createObject($version);
    }

    /**
     * @return GetInterface|PostInterface
     */
    public function groups(string $version)
    {
        return (new Groups($this->token))->createObject($version);
    }

    public function banners(string $version): GetInterface
    {
        return (new Banners($this->token))->createObject($version);
    }

    public function packages(string $version): GetInterface
    {
        return (new Packages($this->token))->createObject($version);
    }
}
