<?php

namespace Artemadr\VkAds\Methods\Plans;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\OneSubclass;
use Artemadr\VkAds\Methods\PostInterface;

/**
 * Class Plans
 * @method GetInterface|PostInterface createObject(string $version)
 */
class Plans extends OneSubclass
{
}
