<?php

namespace Artemadr\VkAds\Methods\Groups;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\OneSubclass;
use Artemadr\VkAds\Methods\PostInterface;

/**
 * Class Groups
 * @method GetInterface|PostInterface createObject(string $version)
 */
class Groups extends OneSubclass
{
}
