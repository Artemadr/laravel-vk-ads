<?php

namespace Artemadr\VkAds\Methods\Groups\V2;

use Artemadr\VkAds\Formatter\JsonFormatter;
use Artemadr\VkAds\Methods\BaseMethod;
use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\PostInterface;

class Groups extends BaseMethod implements GetInterface, PostInterface
{
    private string $method = 'ad_groups.json';

    public function get(array $data = []): object
    {
        $response = $this->callMethodGet($this->method, $data);
        return (new JsonFormatter($response))->getData();
    }

    public function post(array $data = []): object
    {
        $response = $this->callMethodPost($this->method, $data);
        return (new JsonFormatter($response))->getData();
    }
}
