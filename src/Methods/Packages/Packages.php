<?php

namespace Artemadr\VkAds\Methods\Packages;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\OneSubclass;
use Artemadr\VkAds\Methods\PostInterface;

/**
 * Class Packages
 * @method GetInterface|PostInterface createObject(string $version)
 */
class Packages extends OneSubclass
{
}
