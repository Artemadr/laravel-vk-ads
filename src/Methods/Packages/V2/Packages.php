<?php

namespace Artemadr\VkAds\Methods\Packages\V2;

use Artemadr\VkAds\Formatter\JsonFormatter;
use Artemadr\VkAds\Methods\BaseMethod;
use Artemadr\VkAds\Methods\GetInterface;

class Packages extends BaseMethod implements GetInterface
{
    private string $method = 'packages.json';

    public function get(array $data = []): object
    {
        $response = $this->callMethodGet($this->method, $data);
        return (new JsonFormatter($response))->getData();
    }
}
