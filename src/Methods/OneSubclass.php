<?php

namespace Artemadr\VkAds\Methods;

use ReflectionObject;

abstract class OneSubclass
{
    protected string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function createObject(string $version): GetInterface
    {
        $reflection = new ReflectionObject($this);
        $className = $reflection->getNamespaceName() . '\\' . strtoupper($version) . '\\' . $reflection->getShortName();
        return new $className($this->token);
    }
}
