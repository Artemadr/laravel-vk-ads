<?php

namespace Artemadr\VkAds\Methods;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use ReflectionObject;

abstract class BaseMethod
{
    protected const HOST = 'https://ads.vk.com/api/';
    protected string $version;
    protected string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
        $this->version = $this->getVersion();
    }

    protected function getVersion(): string
    {
        return strtolower(substr(strrchr($this->getNamespace(), '\\'), 1));
    }

    protected function getNamespace(): string
    {
        return (new ReflectionObject($this))->getNamespaceName();
    }

    protected function callMethodGet($method, $args): Response
    {
        return Http::withToken($this->token)
            ->get($this->getUrl($method), $args);
    }

    protected function callMethodPost($method, $args): Response
    {
        return Http::withToken($this->token)
            ->post($this->getUrl($method), $args);
    }

    protected function getUrl($method): string
    {
        return static::HOST . $this->version . '/' . $method;
    }
}
