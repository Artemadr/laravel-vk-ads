<?php

namespace Artemadr\VkAds\Methods;

use Illuminate\Support\Str;
use ReflectionObject;

abstract class SeveralSubclasses
{
    protected string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function __call(string $name, array $arguments)
    {
        $className = (new ReflectionObject($this))->getNamespaceName() . '\\' . strtoupper($arguments[0]) . '\\' . Str::studly($name);
        return new $className($this->token);
    }
}
