<?php

namespace Artemadr\VkAds\Methods\Banners\V2;

use Artemadr\VkAds\Formatter\JsonFormatter;
use Artemadr\VkAds\Methods\BaseMethod;
use Artemadr\VkAds\Methods\GetInterface;

class Banners extends BaseMethod implements GetInterface
{
    public function get(array $data = []): object
    {
        $method = 'banners.json';
        $response = $this->callMethodGet($method, $data);
        return (new JsonFormatter($response))->getData();
    }
}
