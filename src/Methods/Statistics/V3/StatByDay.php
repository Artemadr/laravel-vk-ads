<?php

namespace Artemadr\VkAds\Methods\Statistics\V3;

use Artemadr\VkAds\Formatter\JsonFormatter;
use Artemadr\VkAds\Methods\BaseMethod;
use Artemadr\VkAds\Methods\GetInterface;

class StatByDay extends BaseMethod implements GetInterface
{
    public function get(array $data = []): object
    {
        $method = 'statistics/banners/day.json';
        $response = $this->callMethodGet($method, $data);
        return (new JsonFormatter($response))->getData();
    }
}
