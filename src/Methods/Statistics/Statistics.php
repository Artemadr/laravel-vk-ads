<?php

namespace Artemadr\VkAds\Methods\Statistics;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\SeveralSubclasses;

/**
 * Class Statistics
 * @method GetInterface StatByDay(string $version)
 */
class Statistics extends SeveralSubclasses
{
}
