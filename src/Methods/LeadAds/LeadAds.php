<?php

namespace Artemadr\VkAds\Methods\LeadAds;

use Artemadr\VkAds\Methods\GetInterface;
use Artemadr\VkAds\Methods\SeveralSubclasses;

/**
 * Class LeadAds
 * @method GetInterface leadForms(string $version)
 * @method GetInterface leadFormLeadsExport(string $version)
 */
class LeadAds extends SeveralSubclasses
{
}
