<?php

namespace Artemadr\VkAds\Methods\LeadAds\V1;

use Artemadr\VkAds\Formatter\CsvFormatter;
use Artemadr\VkAds\Methods\BaseMethod;
use Artemadr\VkAds\Methods\GetInterface;

class LeadFormLeadsExport extends BaseMethod implements GetInterface
{
    /**
     * @return array|object
     */
    public function get(int $leadFormId, string $exportFormat = 'csv', array $data = [])
    {
        $method = 'lead_ads/lead_forms/' . $leadFormId . '/leads.' . $exportFormat;
        $response = $this->callMethodGet($method, $data);
        return (new CsvFormatter($response))->getData();
    }
}
