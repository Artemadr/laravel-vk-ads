<?php

namespace Artemadr\VkAds\Formatter;

interface FormatterInterface
{
    public function getData();
}
