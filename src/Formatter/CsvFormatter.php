<?php

namespace Artemadr\VkAds\Formatter;

use Illuminate\Http\Client\Response;

class CsvFormatter implements FormatterInterface
{
    private Response $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return array|object
     */
    public function getData()
    {
        if (!$this->response->successful()) {
            return (object)[
                'error' => (object)[
                    'code' => $this->response->status(),
                    'message' => $this->response->body()
                ]
            ];
        }

        $data = $this->response->body();

        $csv = array_map('str_getcsv', explode(PHP_EOL, $data));
        $csv = array_filter($csv, fn($row) => !is_null($row[0]));
        $headingRow = array_shift($csv);
        array_walk($csv, fn(&$row) => $row = is_null($row[0]) ? $row : array_combine($headingRow, $row));

        return $csv;
    }
}
