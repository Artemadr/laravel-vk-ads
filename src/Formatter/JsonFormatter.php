<?php

namespace Artemadr\VkAds\Formatter;

use Illuminate\Http\Client\Response;

class JsonFormatter implements FormatterInterface
{
    private Response $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function getData(): object
    {
        return $this->response->successful() ? $this->response->object() : (object)[
            'error' => (object)[
                'code' => $this->response->status(),
                'message' => $this->response->body()
            ]
        ];
    }
}
